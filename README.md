
Das ist alter kopierter Content. Den Prozess müssten wir noch abstimmen!

# Linked Open Actors (LOA)
This is the [asciidoc](https://asciidoc.org) version of linkedopenactors webiste since Mai 2021.  
It is build with [asciidoctor-maven-plugin](https://github.com/asciidoctor/asciidoctor-maven-plugin).  
See: https://linkedopenactors.org/

# Branching Model
We follow the [git-branching-model](https://nvie.com/posts/a-successful-git-branching-model/) but skip `release branches`!  

# Contribution
* before starting a contribution, contact us via [fairsync chat](https://fairchat.net/channel/fairsync)
* create a feature branch from the develop branch
* create a merge request and assign it to a maintainer

# Branching, merging, releasing
Therefore we are using [gitflow-maven-plugin](https://github.com/aleksandr-m/gitflow-maven-plugin)

# Notes
* http://secondstring.sourceforge.net/
* https://github.com/agreementmaker/secondstring
